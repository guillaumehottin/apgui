#!/bin/bash

VERSION=$(poetry run semantic-release print-version);
if [[ "$VERSION" != "" ]]
then
  sed -i "s/0.0.0/$VERSION/g" pyproject.toml;
  poetry run semantic-release publish;
else
  echo "$VERSION";
fi
