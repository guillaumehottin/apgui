#!/usr/bin/env bash

git config fetch.prune on
git config merge.ff off
git config pull.rebase merges
git config commit.template .commit-msg

pre-commit install
