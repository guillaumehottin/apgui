"""This module export the class DataTransferObject.

DataTransferObject is a parent class for all dataclasses that represent data that can be automatically represented as
dicts or instantiated from dicts.
All class inheriting from it must also be dataclasses.

    Typical usage example:
        @dataclass
        class Movie(DataTransferObject):
            title: str
            release_year: int

        movie = Movie("Star Wars", 1977)
        movie.to_dict() == {"title": "Star Wars", "release_year": 1977}
        Movie.from_dict({"title": "Star Wars", "release_year": 1977}) == Movie("Star Wars", 1977)
"""

import dataclasses
from dataclasses import dataclass

from enum import Enum

import dacite
from dacite import Config

from apgui.error import WrongTypeError, MissingValueError
from apgui.utils.dict import remove_nones


@dataclass
class DataTransferObject:
    """Parent for all classes that need an automatic to_dict and from_dict.

    Every class that inherits from it must have the dataclass decorator.
    """

    def to_dict(self) -> dict:
        """Gets the object as a dict while removing all None values in it.

        Returns: the dict representing the object.
        """
        dto_dict = dataclasses.asdict(self)
        remove_nones(dto_dict)

        return dto_dict

    @classmethod
    def from_dict(cls, dto_dict: dict) -> "DataTransferObject":
        """Gets a DataTransferObject from its dict representation.

        Args:
            dto_dict: the dict representing the DataTransferObject.

        Returns: the DataTransferObject.

        Raises:
            WrongTypeError: a value with the wrong type was provided in the dict
            MissingValueError: a value was not provided in the dict
        """
        try:
            return dacite.from_dict(cls, dto_dict, config=Config(cast=[Enum]))
        except dacite.WrongTypeError as exception:
            try:
                expected_type = exception.field_type.__name__
            except AttributeError:
                expected_type = str(exception.field_type)

            raise WrongTypeError(
                field_path=exception.field_path,
                expected_type=expected_type,
                actual_type=type(exception.value).__name__,
                actual_value=exception.value,
            ) from exception
        except dacite.MissingValueError as exception:
            raise MissingValueError(exception.field_path) from exception
