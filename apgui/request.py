"""This module exports a base ValidRequest and base InvalidRequest used in use cases.

ValidRequest must be inherited to add the values to be passed to the use case.
All classes inheriting ValidRequest must also be dataclasses.
A valid request is always True.

    Typical usage example:
        @dataclass
        CreateMovieValidRequest(ValidRequest):
            title: str
            release_year: int

        request = CreateMovieValidRequest("Star Wars", 1977)
        request is True

InvalidRequest is used as is, it should not be inherited. When instantiating an InvalidRequest, the error must be
provided, so it can be returned to the caller of the use case.
An InvalidRequest is always False.

    Typical usage example:
        request = InvalidRequest(SystemFailureError())
        request is False
"""

from dataclasses import dataclass

from apgui.dto import DataTransferObject
from apgui.error import Error


@dataclass
class ValidRequest(DataTransferObject):
    """ValidRequest for a use case.

    It must be inherited to provide the data that is provided to the corresponding use case.
    """

    def __bool__(self):
        return True


@dataclass
class InvalidRequest(DataTransferObject):
    """This class represents an invalid request for a use case.

    An error must be provided to be returned to the called or the use case.
    """

    error: Error

    def __bool__(self):
        return False
