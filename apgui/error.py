"""This module exports a base Error that every exception must inherit from and generic exceptions.

Error allows to get the type of the error as a snake case string, the human-readable message for the error and the
details of the error as a dict with all the relevant information.

    Typical usage example:
        class FileNotFoundError(Error):

            def __init__(self, file_path: str):
                self._file_path = file_path

            @property
            def type(self) -> str:
                return "file_not_found_error"

            @property
            def message(self) -> str:
                return f"No file was found at path {file_path!r}."

            @property
            def details(self) -> dict[str]:
                return {
                    "file_path": self._file_path
                }
"""

from abc import abstractmethod, ABC
from dataclasses import dataclass


class Error(Exception, ABC):
    """Base Error that every exception must inherit from.

    Every class that inherits from it must implement type and message as properties.
    If any information is relevant to the error, the details property must also be implemented.
    """

    def __new__(cls, *args, **kwargs):
        """Allows to have an abstract exception, which is not possible out of the box."""
        res = super().__new__(cls, *args, **kwargs)
        if hasattr(cls, "__abstractmethods__") and cls.__abstractmethods__:
            raise TypeError(
                f"Can't instantiate abstract class {cls.__name__} with abstract methods "
                f"{','.join(sorted(cls.__abstractmethods__))}"
            )
        return res

    def __init__(self):
        super().__init__(self.message)

    @property
    @abstractmethod
    def type(self) -> str:
        """Gets the snake case type of the error.

        Returns: the str snake case type of the error.
        """

    @property
    @abstractmethod
    def message(self):
        """Get the human-readable message for the error.

        Returns: a str that represents the error that can be easily read by a human.
        """

    @property
    @abstractmethod
    def details(self) -> dict[str] | None:
        """Gets the details of the error.

        Returns: a dict with all the details of the error.

        """


class SystemFailureError(Error):
    """An error raised when there was a system failure (usually an IO operation failed)."""

    @property
    def type(self) -> str:
        return "system_failure"

    @property
    def message(self) -> str:
        return "An unknown error occurred."

    @property
    def details(self) -> dict[str] | None:
        return None


class MissingValueError(Error):
    """An error raised when a value was not provided (usually in a dict provided by the user)."""

    def __init__(self, field_path: str):
        self._field_path = field_path

        super().__init__()

    @property
    def type(self) -> str:
        return "missing_value"

    @property
    def message(self) -> str:
        return f"Field {self._field_path!r} is required."

    @property
    def details(self) -> dict[str] | None:
        return {"field_path": self._field_path}


@dataclass
class WrongTypeError(Error):
    """An error raised when a value of the wrong type was provided (usually in a dict provided by the user)."""

    def __init__(self, field_path: str, expected_type: str, actual_type: str, actual_value):
        self._field_path = field_path
        self._expected_type = expected_type
        self._actual_type = actual_type
        self._actual_value = actual_value

        super().__init__()

    @property
    def type(self) -> str:
        return "wrong_type"

    @property
    def message(self) -> str:
        return (
            f"Field {self._field_path!r} expected value of type {self._expected_type!r}. "
            f"Got {self._actual_value!r} of type {self._actual_type!r} instead."
        )

    @property
    def details(self) -> dict[str] | None:
        return {
            "field_path": self._field_path,
            "expected_type": self._expected_type,
            "actual_type": self._actual_type,
            "actual_value": self._actual_value,
        }


class NotFoundError(Error):
    """An error raised when an entity was not found."""

    def __init__(self, entity: str, _id: int | str | tuple[int | str, ...]):
        self._entity = entity
        self._id = _id

        super().__init__()

    @property
    def type(self) -> str:
        return "not_found"

    @property
    def message(self) -> str:
        return f"Entity {self._entity!r} with id {self._id!r} was not found."

    @property
    def details(self) -> dict[str] | None:
        return {"entity": self._entity, "id": self._id}


class AlreadyExistsError(Error):
    """An error raised when an entity already exists (usually when trying to create one)."""

    def __init__(self, entity: str, _id: int | str | tuple[int | str, ...]):
        self._entity = entity
        self._id = _id

        super().__init__()

    @property
    def type(self) -> str:
        return "already_exists"

    @property
    def message(self) -> str:
        return f"Entity {self._entity!r} with id {self._id!r} already exists."

    @property
    def details(self) -> dict[str] | None:
        return {"entity": self._entity, "id": self._id}


class InvalidCredentialsError(Error):
    """An error raised when the provided credentials where incorrect."""

    @property
    def type(self) -> str:
        return "invalid_credentials"

    @property
    def message(self) -> str:
        return "Invalid credentials."

    @property
    def details(self) -> dict[str] | None:
        return None


class UnauthorizedError(Error):
    """An error raised when access to a protected resource is requested."""

    @property
    def type(self) -> str:
        return "unauthorized"

    @property
    def message(self) -> str:
        return "Access to this resource is unauthorized."

    @property
    def details(self) -> dict[str] | None:
        return None


class UnauthorizedActionError(Error):
    """An error raised when trying to perform a protected action."""

    @property
    def type(self) -> str:
        return "unauthorized_action"

    @property
    def message(self) -> str:
        return "You don't have the necessary authorizations to perform this action."

    @property
    def details(self) -> dict[str] | None:
        return None
