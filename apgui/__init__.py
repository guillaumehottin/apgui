"""This module holds a placeholder for the version. This should not be changed as it is handled by semantic release."""
__version__ = "0.0.0"
