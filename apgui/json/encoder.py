"""This module exports the GuiSONEncoder JSON encoder.

The GuiSONEncoder allows encoding Python objects to JSON objects, including all standard supported types as well as
datetimes and DataTransferObjects.
It can also be subclassed to provide a mapping between DataTransferObjects and fields that must be excluded when
encoding.

    Typical usage examples:

    json.dumps(value, cls=GuiSONEncoder)

    class Encoder(GuiSONEncoder):

        @property
        def class_excluded_fields_mapping(self) -> dict[type[DataTransferObject], list[str]]:
            return {Movie: ["title", "release_date"]}

    json.dumps(value, cls=Encoder)
"""


from datetime import datetime
from json import JSONEncoder

from apgui.dto import DataTransferObject
from apgui.json.utils import dto_to_json_dict, datetime_to_json_dict


class GuiSONEncoder(JSONEncoder):
    """JSONEncoder allowing encoding of standard types as well as datetime objects.

    It allows providing of a mapping between DataTransferObjects and their fields that must be excluded when
    serializing to JSON
    """

    def default(self, o):  # pylint: disable=R1710
        if isinstance(o, DataTransferObject):
            return dto_to_json_dict(o, self.class_excluded_fields_mapping)
        if isinstance(o, datetime):
            return datetime_to_json_dict(o)

    @property
    def class_excluded_fields_mapping(self) -> dict[type[DataTransferObject], list[str]]:
        """Provides the mapping between DataTransferObjects and the fields that must be excluded.

        Returns:
            A dict mapping DataTransferObjects to the list of the names of fields that must be excluded. For example:

            {Movie: ["title", "release_date"], Actor: ["date_of_birth"]}
        """
        return {}
