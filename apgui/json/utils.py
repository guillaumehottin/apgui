"""This module exports utility functions to allow JSON encoding and decoding."""


import dataclasses
from datetime import datetime


from apgui.dto import DataTransferObject
from apgui.json.types import DatetimeJSONDict
from apgui.utils.datetime import get_utc_datetime_from_timestamp, get_timestamp_from_datetime
from apgui.utils.dict import remove_fields


def datetime_from_json_dict(json_dict: DatetimeJSONDict) -> datetime:
    """Gets a datetime from its JSON representation.

    Args:
        json_dict: the JSON representation of the datetime.

    Returns: the datetime object.
    """
    return get_utc_datetime_from_timestamp(json_dict["timestamp_s"])


def dict_to_json_dict(
    value_dict: dict[str], excluded_fields_mapping: dict[type[DataTransferObject], list[str]]
) -> dict:
    """Gets the JSON dict of a dict. It allows for serialization of its values.

    Args:
        value_dict: the dict to serialize.
        excluded_fields_mapping: a dict that maps a DTO to a list of its field names to exclude.

    Returns: the serialized dict.
    """
    return {key: any_to_json_dict(value, excluded_fields_mapping) for key, value in value_dict.items()}


def list_to_json_list(value_list: list, excluded_fields_mapping: dict[type[DataTransferObject], list[str]]) -> list:
    """Gets the JSON dict of a list. It allows for serialization of its values.

    Args:
        value_list: the list to serialize.
        excluded_fields_mapping: a dict that maps a DTO to a list of its field names to exclude.

    Returns: the serialized list.
    """
    return [any_to_json_dict(value, excluded_fields_mapping) for value in value_list]


def datetime_to_json_dict(value: datetime) -> DatetimeJSONDict:
    """Gets the JSON dict of a datetime.

    Args:
        value: the datetime object.

    Returns: the JSON datetime dict.
    """
    return {"__type__": "datetime", "timestamp_s": get_timestamp_from_datetime(value)}


def any_to_json_dict(
    value: dict[str] | list | tuple | str | int | float | bool | DataTransferObject | datetime | None,
    excluded_fields_mapping: dict[type[DataTransferObject], list[str]],
):
    """Gets the JSON representation of any supported types.

    Args:
        value: the value to serialize.
        excluded_fields_mapping: a dict that maps a DTO to a list of its field names to exclude.

    Returns: the JSON serialized value.
    """
    if isinstance(value, DataTransferObject):
        return dto_to_json_dict(value, excluded_fields_mapping)
    if isinstance(value, dict):
        return dict_to_json_dict(value, excluded_fields_mapping)
    if isinstance(value, list):
        return list_to_json_list(value, excluded_fields_mapping)
    if isinstance(value, datetime):
        return datetime_to_json_dict(value)

    return value


def dto_to_json_dict(
    dto: "DataTransferObject", excluded_fields_mapping: dict[type[DataTransferObject], list[str]]
) -> dict:
    """Gets the JSON dict of a DataTransferObject.

    Args:
        dto: the DataTransferObject to serialize.
        excluded_fields_mapping: a dict that maps a DTO to a list of its field names to exclude.

    Returns: the JSON serialized DTO.
    """
    dto_dict = {
        field.name: any_to_json_dict(getattr(dto, field.name), excluded_fields_mapping)
        for field in dataclasses.fields(dto)
    }

    remove_fields(dto_dict, excluded_fields_mapping.get(type(dto), []))

    return dto_dict
