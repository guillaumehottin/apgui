"""This module exports the models for serializing and deserializing complex Python types.

All types are represented as a dict, with the key "__type__" and other keys relevant to this type for deserialization.

DatetimeJSONDict represents a datetime. Its __type__ is datetime and its value is timestamp_s, a float being its UTC
timestamp in seconds.

    Typical usage example:
        {
            "__type__": "datetime",
            "timestamp_s": 1645556315.546844
        }
"""

from typing import TypedDict, Literal


class DatetimeJSONDict(TypedDict):
    """TypedDict defining how a datetime is serialized in JSON.

    Attributes:
        __type__: The constant string "datetime".
        timestamp_s: The float timestamp in seconds.
    """

    __type__: Literal["datetime"]
    timestamp_s: float
