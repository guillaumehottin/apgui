"""This module exports the GuiSONDecoder JSON decoder.

The GuiSONDecoder allows decoding JSON objects to python dicts, including all standard supported types as well as
datetimes.

    Typical usage example:

    json.loads(json_string, cls=GuiSONDecoder)
"""

import json
from json import JSONDecoder

from apgui.json.utils import datetime_from_json_dict


class GuiSONDecoder(JSONDecoder):
    """JSONDecoder allowing decoding of standard types as well as datetimes.

    This JSON decoder allows decoding of all standard types as well as datetimes, which are represented as the
    following dict:

        {"__type__": "datetime", "timestamp_s": 1645503469}
    """

    def __init__(self, *args, **kwargs):
        json.JSONDecoder.__init__(self, object_hook=self.object_hook, *args, **kwargs)

    @staticmethod
    def object_hook(json_object):  # pylint: disable=E0202
        """Translate JSON dicts that may contain a special type into standard Python types.


        Args:
            json_object: a standard JSON object.

        Returns: an object (list, dict or standard type) comprised of standard types.

        """
        if isinstance(json_object, dict):
            if "__type__" in json_object:
                __type__ = json_object["__type__"]

                if __type__ == "datetime":
                    return datetime_from_json_dict(json_object)

            return {key: GuiSONDecoder.object_hook(value) for key, value in json_object.items()}

        if isinstance(json_object, list):
            return [GuiSONDecoder.object_hook(value) for value in json_object]

        return json_object
