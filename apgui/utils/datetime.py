"""This module exports utility functions to deal with datetimes."""

from datetime import datetime

import pytz


def get_utc_now() -> datetime:
    """Gets the datetime for this instant with a UTC timezone.

    Returns: a UTC datetime.
    """
    return datetime.now(tz=pytz.UTC)


def get_timestamp_from_datetime(datetime_value: datetime) -> float:
    """Gets the timestamp from a datetime.

    Args:
        datetime_value: a datetime

    Returns: the float timestamp of the datetime in seconds.
    """

    return datetime_value.timestamp()


def get_utc_datetime_from_timestamp(timestamp_s: float) -> datetime:
    """Gets a datetime from a timestamp with a UTC timezone.

    Args:
        timestamp_s: a UNIX timestamp in seconds.

    Returns: the datetime corresponding to the timestamp with a UTC timezone.
    """
    return datetime.fromtimestamp(timestamp_s, pytz.utc)
