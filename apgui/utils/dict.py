"""This module exports utility functions to deal with dicts."""

from collections.abc import Collection


def remove_nones(model_dict: dict) -> None:
    """Removes None values for a dict recursively.

    If a value is a list, it will be called for all elements of that list.
    Any empty resulting list or dict will be removed too. This is done in place.

    Args:
        model_dict: the dict from which to remove Nones.
    """
    # Get keys from root level that are None
    root_level_none_keys = [key for key, value in model_dict.items() if value is None]

    # Remove None values
    for key in root_level_none_keys:
        model_dict.pop(key)

    # Check all remaining non None values
    for dict_value in model_dict.values():
        if isinstance(dict_value, list):
            # List case
            for list_value in dict_value:
                # Dict in list case
                if isinstance(list_value, dict):
                    remove_nones(list_value)

            # Remove all Nones in list
            while None in dict_value:
                dict_value.remove(None)

            # Remove all empty dicts in list
            while {} in dict_value:
                dict_value.remove({})

        if isinstance(dict_value, dict):
            # Nested dict case
            remove_nones(dict_value)

    # Get all keys which are empty dicts and lists
    empty_dict_or_list_keys = [key for key, value in model_dict.items() if value in [{}, []]]

    # Remove empty dicts and lists
    for key in empty_dict_or_list_keys:
        model_dict.pop(key)


def remove_fields(model_dict: dict, fields: Collection[str]) -> None:
    """
    Remove fields from a given dict in place.

    Args:
        model_dict: the dict to remove fields.
        fields: the list of field names to remove.
    """
    for field in fields:
        model_dict.pop(field)
