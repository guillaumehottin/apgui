"""This module exports a base ResponseSuccess and ResponseFailure returned by use cases.

ResponseSuccess is a generic class and can be used as is by providing the type of the value returned by the use case.
A ResponseSuccess is always True.

    Typical usage example:
        response = ResponseSuccess(movie)
        response.value == movie
        response is True

ResponseFailure can be used as is by providing it the error that occurred in the use case.
A ResponseFailure is always False.

    Typical usage example:
        response = ResponseFailure(error)
        response.value == {"type": "system_failure_error", "message": "An unknown error occurred."}
        response.error == error
        response is False
"""

from typing import TypeVar, Generic

from apgui.error import Error
from apgui.request import InvalidRequest

Type = TypeVar("Type")


class ResponseSuccess(Generic[Type]):
    """This class represents a valid response from a use case with its associated value."""

    value: Type

    def __init__(self, value: Type = None):
        self.value: Type = value

    def __bool__(self):
        return True


class ResponseFailure:
    """This class represents an invalid response from a use case with its associated error."""

    def __init__(self, error: Error):
        self.error = error

    @property
    def value(self) -> dict[str, str]:
        """Gets the value of the invalid response. It consists of the type, message and details of the associated error.

        Returns: a dict with the type, message and if present details of the error
        """
        value = {
            "type": self.error.type,
            "message": self.error.message,
        }

        details = self.error.details

        if details is not None:
            value["details"] = details

        return value

    def __bool__(self):
        return False


def build_response_from_invalid_request(request: InvalidRequest) -> ResponseFailure:
    """Builds a ResponseFailure from an InvalidRequest.

    It uses the error that occurred while building the request as its error.

    Args:
        request: an InvalidRequest.

    Returns: a ResponseFailure with the error associated to the request.
    """
    return ResponseFailure(request.error)
