from dataclasses import dataclass, is_dataclass
import pytest

from apgui.dto import DataTransferObject
from apgui.error import WrongTypeError, MissingValueError


@dataclass
class NestedDTO(DataTransferObject):

    simple_dto: "SimpleDTO"
    best: str


@dataclass
class SimpleDTO(DataTransferObject):

    ekip: str
    name: str
    optional: str | None = None


@pytest.fixture
def simple_dto():
    return SimpleDTO(ekip="667", name="ekip")


@pytest.fixture
def nested_dto() -> NestedDTO:
    return NestedDTO(simple_dto=SimpleDTO(ekip="667", name="ekip"), best="batman")


def test_dto_is_dataclass():
    dto = DataTransferObject()

    assert is_dataclass(dto)


def test_simple_dto_init():
    simple_dto = SimpleDTO(ekip="667", name="ekip", optional="optional")

    assert simple_dto.ekip == "667"
    assert simple_dto.name == "ekip"
    assert simple_dto.optional == "optional"


def test_simple_dto_init_no_optional():
    simple_dto = SimpleDTO(ekip="667", name="ekip")

    assert simple_dto.ekip == "667"
    assert simple_dto.name == "ekip"
    assert simple_dto.optional is None


def test_dto_equals():
    simple_dto_1 = SimpleDTO(ekip="667", name="ekip")

    simple_dto_2 = SimpleDTO(ekip="667", name="ekip")

    assert simple_dto_1 == simple_dto_2


def test_simple_dto_to_dict(simple_dto):
    simple_dto_dict = simple_dto.to_dict()

    assert simple_dto_dict == {"ekip": "667", "name": "ekip"}


def test_nested_dto_to_dict(nested_dto):
    nested_dto_dict = nested_dto.to_dict()

    assert nested_dto_dict == {"best": "batman", "simple_dto": {"ekip": "667", "name": "ekip"}}


def test_simple_dto_from_dict():
    simple_dto_dict = {"ekip": "667", "name": "ekip"}

    simple_dto = SimpleDTO.from_dict(simple_dto_dict)

    assert simple_dto == SimpleDTO(ekip="667", name="ekip")


def test_nested_dto_from_dict():
    nested_dto_dict = {"best": "batman", "simple_dto": {"ekip": "667", "name": "ekip"}}

    nested_dto = NestedDTO.from_dict(nested_dto_dict)

    assert nested_dto == NestedDTO(best="batman", simple_dto=SimpleDTO(ekip="667", name="ekip"))


def test_dto_from_dict_wrong_type():
    dto_dict = {"ekip": 667, "name": "ekip"}

    with pytest.raises(WrongTypeError):
        SimpleDTO.from_dict(dto_dict)


def test_dto_from_dict_optional_wrong_type():
    dto_dict = {"ekip": "667", "name": "ekip", "optional": False}

    with pytest.raises(WrongTypeError):
        SimpleDTO.from_dict(dto_dict)


def test_dto_from_dict_missing_value():
    dto_dict = {"ekip": "667"}

    with pytest.raises(MissingValueError):
        SimpleDTO.from_dict(dto_dict)
