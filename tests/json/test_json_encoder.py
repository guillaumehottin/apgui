import json
from dataclasses import dataclass
from datetime import datetime

import pytz

from apgui.dto import DataTransferObject
from apgui.json.encoder import GuiSONEncoder


@dataclass
class SimpleDTO(DataTransferObject):
    a: int
    b: str


@dataclass
class NestedDTO(DataTransferObject):
    a: list[SimpleDTO]
    b: dict[str, SimpleDTO]
    c: SimpleDTO


class ExcludeEncoder(GuiSONEncoder):
    @property
    def class_excluded_fields_mapping(self) -> dict[type[DataTransferObject], list[str]]:
        return {SimpleDTO: ["a"]}


def test_simple_encoder():
    test_dict = {
        "a": 1,
        "b": 2.5,
        "c": "test",
        "d": {"a": 85},
        "e": [SimpleDTO(5, "e")],
        "f": datetime(2022, 2, 21, 18, 54, 56, 954652, tzinfo=pytz.utc),
    }

    json_string = json.dumps(test_dict, cls=GuiSONEncoder)

    assert json.loads(json_string) == {
        "a": 1,
        "b": 2.5,
        "c": "test",
        "d": {"a": 85},
        "e": [{"a": 5, "b": "e"}],
        "f": {"__type__": "datetime", "timestamp_s": 1645469696.954652},
    }


def test_exclude_encoder():
    test_dict = {"a": 1, "b": 2.5, "c": "test", "d": {"a": 85}, "e": SimpleDTO(5, "e")}

    json_string = json.dumps(test_dict, cls=ExcludeEncoder)

    assert json.loads(json_string) == {"a": 1, "b": 2.5, "c": "test", "d": {"a": 85}, "e": {"b": "e"}}
