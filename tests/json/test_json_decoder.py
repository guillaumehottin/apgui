import json
from datetime import datetime

import pytz

from apgui.json.decoder import GuiSONDecoder


def test_guison_decoder():
    test_json = """{
        "a": 1,
        "b": 2.5,
        "c": "test",
        "d": {"a": 85},
        "e": [1, 2, 3],
        "f": {"__type__": "datetime", "timestamp_s": 1645469696.954652}
    }"""

    test_dict = json.loads(test_json, cls=GuiSONDecoder)

    assert test_dict == {
        "a": 1,
        "b": 2.5,
        "c": "test",
        "d": {"a": 85},
        "e": [1, 2, 3],
        "f": datetime(2022, 2, 21, 18, 54, 56, 954652, tzinfo=pytz.utc),
    }
