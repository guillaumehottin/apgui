from dataclasses import dataclass

from apgui.dto import DataTransferObject
from apgui.json.utils import dto_to_json_dict


@dataclass
class SimpleDTO(DataTransferObject):
    a: int
    b: str


@dataclass
class NestedDTO(DataTransferObject):
    a: list[SimpleDTO]
    b: dict[str, SimpleDTO]
    c: SimpleDTO


def test_dto_to_json_dict_simple():
    dto = SimpleDTO(5, "test")

    json_dto = dto_to_json_dict(dto, {})

    assert json_dto == {"a": 5, "b": "test"}


def test_dto_to_json_dict_exclude():
    dto = SimpleDTO(5, "test")

    json_dto = dto_to_json_dict(dto, {SimpleDTO: ["b"]})

    assert json_dto == {"a": 5}


def test_dto_to_json_dict_nested():
    dto = NestedDTO(
        [SimpleDTO(1, "1"), SimpleDTO(2, "2")], {"3": SimpleDTO(3, "3"), "4": SimpleDTO(4, "4")}, SimpleDTO(5, "5")
    )

    json_dto = dto_to_json_dict(dto, {})

    assert json_dto == {
        "a": [{"a": 1, "b": "1"}, {"a": 2, "b": "2"}],
        "b": {"3": {"a": 3, "b": "3"}, "4": {"a": 4, "b": "4"}},
        "c": {"a": 5, "b": "5"},
    }


def test_dto_to_json_dict_nested_exclude():
    dto = NestedDTO(
        [SimpleDTO(1, "1"), SimpleDTO(2, "2")], {"3": SimpleDTO(3, "3"), "4": SimpleDTO(4, "4")}, SimpleDTO(5, "5")
    )

    json_dto = dto_to_json_dict(dto, {NestedDTO: ["b"], SimpleDTO: ["a"]})

    assert json_dto == {
        "a": [{"b": "1"}, {"b": "2"}],
        "c": {"b": "5"},
    }
