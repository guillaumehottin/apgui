from apgui.json.utils import dict_to_json_dict


def test_dict_to_json_dict():
    test_dict = {"a": 1, "b": "salut", "c": 6.67, "d": True, "e": None}

    json_dict = dict_to_json_dict(test_dict, {})

    assert json_dict == {"a": 1, "b": "salut", "c": 6.67, "d": True, "e": None}


def test_dict_to_json_dict_nested():
    test_dict = {
        "a": 1,
        "b": "salut",
        "c": 6.67,
        "d": True,
        "e": None,
        "f": {"a": 1, "b": "salut", "c": 6.67, "d": True, "e": None},
    }

    json_dict = dict_to_json_dict(test_dict, {})

    assert json_dict == {
        "a": 1,
        "b": "salut",
        "c": 6.67,
        "d": True,
        "e": None,
        "f": {"a": 1, "b": "salut", "c": 6.67, "d": True, "e": None},
    }
