from datetime import datetime

import pytz

from apgui.json.utils import datetime_from_json_dict


def test_datetime_from_json_dict():
    datetime_json_dict = {"__type__": "datetime", "timestamp_s": 1645469696.954652}

    test_datetime = datetime_from_json_dict(datetime_json_dict)

    assert test_datetime == datetime(2022, 2, 21, 18, 54, 56, 954652, tzinfo=pytz.utc)
