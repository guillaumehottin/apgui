from datetime import datetime

import pytz

from apgui.json.utils import datetime_to_json_dict


def test_datetime_to_json_dict():
    test_datetime = datetime(2022, 2, 21, 18, 54, 56, 954652, tzinfo=pytz.utc)

    datetime_json_dict = datetime_to_json_dict(test_datetime)

    assert datetime_json_dict == {"__type__": "datetime", "timestamp_s": 1645469696.954652}
