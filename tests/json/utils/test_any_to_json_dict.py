from dataclasses import dataclass
from datetime import datetime
from unittest.mock import patch

import pytz

from apgui.dto import DataTransferObject
from apgui.json.utils import any_to_json_dict


@patch("apgui.json.utils.dto_to_json_dict")
def test_any_to_json_dict_dto(dto_to_json_dict_mock):
    @dataclass
    class SimpleDTO(DataTransferObject):
        a: int

    dto_to_json_dict_mock.return_value = {"a": 5}

    dto = SimpleDTO(5)

    dto_json_dict = any_to_json_dict(dto, {})

    assert dto_json_dict == {"a": 5}
    dto_to_json_dict_mock.assert_called_with(dto, {})


@patch("apgui.json.utils.dict_to_json_dict")
def test_any_to_json_dict_dict(dict_to_json_dict_mock):
    dict_to_json_dict_mock.return_value = {"a": 5}

    test_dict = {"a": 5}

    dict_json_dict = any_to_json_dict(test_dict, {})

    assert dict_json_dict == {"a": 5}
    dict_to_json_dict_mock.assert_called_with(test_dict, {})


@patch("apgui.json.utils.list_to_json_list")
def test_any_to_json_dict_list(list_to_json_list_mock):
    list_to_json_list_mock.return_value = [1, "a"]

    test_list = [1, "a"]

    list_json_dict = any_to_json_dict(test_list, {})

    assert list_json_dict == [1, "a"]
    list_to_json_list_mock.assert_called_with([1, "a"], {})


@patch("apgui.json.utils.datetime_to_json_dict")
def test_any_to_json_dict_datetime(datetime_to_json_dict_mock):
    datetime_to_json_dict_mock.return_value = {"__type__": "datetime", "timestamp_s": 1645469696.954652}

    test_datetime = datetime(2022, 2, 21, 18, 54, 56, 954652, tzinfo=pytz.utc)

    datetime_json_dict = any_to_json_dict(test_datetime, {})

    assert datetime_json_dict == {"__type__": "datetime", "timestamp_s": 1645469696.954652}
    datetime_to_json_dict_mock.assert_called_with(test_datetime)
