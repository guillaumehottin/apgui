from apgui.json.utils import list_to_json_list


def test_list_to_json_list():
    test_list = ["a", 1, 2.3, True, None]

    json_list = list_to_json_list(test_list, {})

    assert json_list == ["a", 1, 2.3, True, None]


def test_list_to_json_list_nested():
    test_list = ["a", 1, 2.3, True, None, ["a", 1, 2.3, True, None]]

    json_list = list_to_json_list(test_list, {})

    assert json_list == ["a", 1, 2.3, True, None, ["a", 1, 2.3, True, None]]
