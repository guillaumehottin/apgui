from apgui.utils.dict import remove_nones


def test_remove_nones_with_empty_dict():
    my_dict = {}

    remove_nones(my_dict)

    assert my_dict == {}


def test_remove_nones_not_nested():
    my_dict = {"ekip": None, "name": "ekip"}

    remove_nones(my_dict)

    assert my_dict == {"name": "ekip"}


def test_remove_nones_nested():
    my_dict = {
        "ekip": None,
        "name": "ekip",
        "points": [{"status": "waiting", "actions": {"done": None, "in_progress": 5}}],
    }

    remove_nones(my_dict)

    assert my_dict == {
        "name": "ekip",
        "points": [{"status": "waiting", "actions": {"in_progress": 5}}],
    }


def test_remove_nones_nested_remove_all():
    my_dict = {
        "ekip": None,
        "name": "ekip",
        "points": [{"status": "waiting", "actions": {"done": None, "in_progress": None}}],
    }

    remove_nones(my_dict)

    assert my_dict == {"name": "ekip", "points": [{"status": "waiting"}]}


def test_remove_nones_empty_list():
    my_dict = {"name": "ekip", "my_list": [{"salut": None}, None]}

    remove_nones(my_dict)

    assert my_dict == {"name": "ekip"}
