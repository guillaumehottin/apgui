from apgui.utils.dict import remove_fields


def test_remove_fields():
    test_dict = {"a": 1, "b": 2, "c": 3}

    remove_fields(test_dict, ["a", "c"])

    assert test_dict == {"b": 2}
