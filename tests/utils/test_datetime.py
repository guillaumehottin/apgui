from datetime import datetime

import pytz

from apgui.utils.datetime import get_utc_now, get_timestamp_from_datetime, get_utc_datetime_from_timestamp


def test_get_utc_now():
    utc_now = get_utc_now()

    assert utc_now.tzinfo == pytz.utc


def test_get_timestamp_from_datetime():
    test_datetime = pytz.timezone("Europe/Paris").localize(datetime(2022, 2, 21, 19, 54, 56, 954652))

    timestamp = get_timestamp_from_datetime(test_datetime)

    assert timestamp == 1645469696.954652


def test_get_datetime_from_utc_timestamp():
    timestamp = 1645469696.954652

    test_datetime = get_utc_datetime_from_timestamp(timestamp)

    assert test_datetime == datetime(2022, 2, 21, 18, 54, 56, 954652, tzinfo=pytz.utc)
    assert test_datetime.tzinfo == pytz.utc
