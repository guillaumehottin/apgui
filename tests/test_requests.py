from dataclasses import is_dataclass

import pytest

from apgui.dto import DataTransferObject
from apgui.error import SystemFailureError
from apgui.request import ValidRequest, InvalidRequest


@pytest.fixture
def valid_request():
    return ValidRequest()


@pytest.fixture
def invalid_request():
    return InvalidRequest(error=SystemFailureError())


def test_valid_request_is_dto(valid_request):
    assert isinstance(valid_request, DataTransferObject)


def test_valid_request_is_dataclass(valid_request):
    assert is_dataclass(valid_request)


def test_valid_request_is_true(valid_request):
    assert bool(valid_request) is True


def test_invalid_request_init():
    error = SystemFailureError()

    request = InvalidRequest(error=error)

    assert request.error == error


def test_invalid_request_is_dto(invalid_request):
    assert isinstance(invalid_request, DataTransferObject)


def test_invalid_request_is_dataclass(invalid_request):
    assert is_dataclass(invalid_request)


def test_invalid_request_is_false(invalid_request):
    assert bool(invalid_request) is False
