from apgui.error import SystemFailureError, WrongTypeError
from apgui.request import InvalidRequest
from apgui.response import ResponseSuccess, ResponseFailure, build_response_from_invalid_request


def test_response_success_init():
    response = ResponseSuccess("ekip")

    assert response.value == "ekip"


def test_response_success_is_true():
    response = ResponseSuccess("ekip")

    assert bool(response) is True


def test_response_failure_init():
    error = SystemFailureError()

    response = ResponseFailure(error)

    assert response.error == error


def test_response_failure_value():
    error = WrongTypeError(
        field_path="field", expected_type="expected_type", actual_type="actual_type", actual_value="actual_value"
    )

    response = ResponseFailure(error)

    assert response.value == {"type": error.type, "message": error.message, "details": error.details}


def test_response_failure_is_false():
    error = SystemFailureError()

    response = ResponseFailure(error)

    assert bool(response) is False


def test_build_response_from_invalid_request():
    error = WrongTypeError(
        field_path="field", expected_type="expected_type", actual_type="actual_type", actual_value="actual_value"
    )

    request = InvalidRequest(error)

    response = build_response_from_invalid_request(request)

    assert bool(response) is False
    assert response.error == error
