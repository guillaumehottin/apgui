import pytest

from apgui.error import (
    Error,
    WrongTypeError,
    SystemFailureError,
    MissingValueError,
    NotFoundError,
    AlreadyExistsError,
    InvalidCredentialsError,
    UnauthorizedError,
    UnauthorizedActionError,
)


def test_error_is_exception():
    assert issubclass(Error, Exception)


def test_error_cant_be_instantiated():
    with pytest.raises(TypeError):
        Error()


def test_system_failure_error_is_error():
    assert issubclass(SystemFailureError, Error)


def test_system_failure_error_type():
    error = SystemFailureError()

    assert error.type == "system_failure"


def test_system_failure_error_message():
    error = SystemFailureError()

    assert error.message == "An unknown error occurred."


def test_system_failure_error_details():
    error = SystemFailureError()

    assert error.details is None


def test_missing_value_error_is_error():
    assert issubclass(MissingValueError, Error)


def test_missing_value_error_type():
    error = MissingValueError("field")

    assert error.type == "missing_value"


def test_missing_value_error_message():
    error = MissingValueError("field")

    assert error.message == "Field 'field' is required."


def test_missing_value_error_details():
    error = MissingValueError("field")

    assert error.details == {"field_path": "field"}


def test_wrong_type_error_is_error():
    assert issubclass(WrongTypeError, Error)


def test_wrong_type_error_type():
    error = WrongTypeError(
        field_path="field", expected_type="expected_type", actual_type="actual_type", actual_value="actual_value"
    )

    assert error.type == "wrong_type"


def test_wrong_type_error_message():
    error = WrongTypeError(
        field_path="field", expected_type="expected_type", actual_type="actual_type", actual_value="actual_value"
    )

    assert (
        error.message == "Field 'field' expected value of type 'expected_type'. "
        "Got 'actual_value' of type 'actual_type' instead."
    )


def test_wrong_type_error_details():
    error = WrongTypeError(
        field_path="field", expected_type="expected_type", actual_type="actual_type", actual_value="actual_value"
    )

    assert error.details == {
        "field_path": "field",
        "expected_type": "expected_type",
        "actual_type": "actual_type",
        "actual_value": "actual_value",
    }


def test_not_found_error_is_error():
    assert issubclass(NotFoundError, Error)


def test_not_found_error_type():
    error = NotFoundError("entity", "id")

    assert error.type == "not_found"


def test_not_found_error_message():
    error = NotFoundError("entity", "id")

    assert error.message == "Entity 'entity' with id 'id' was not found."


def test_not_found_error_details():
    error = NotFoundError("entity", "id")

    assert error.details == {"entity": "entity", "id": "id"}


def test_already_exists_is_error():
    assert issubclass(AlreadyExistsError, Error)


def test_already_exists_error_type():
    error = AlreadyExistsError("entity", "id")

    assert error.type == "already_exists"


def test_already_exists_error_message():
    error = AlreadyExistsError("entity", "id")

    assert error.message == "Entity 'entity' with id 'id' already exists."


def test_already_exists_error_get_details():
    error = AlreadyExistsError("entity", "id")

    assert error.details == {"entity": "entity", "id": "id"}


def test_invalid_credentials_error_is_error():
    assert issubclass(InvalidCredentialsError, Error)


def test_invalid_credentials_error_type():
    error = InvalidCredentialsError()

    assert error.type == "invalid_credentials"


def test_invalid_credentials_error_message():
    error = InvalidCredentialsError()

    assert error.message == "Invalid credentials."


def test_invalid_credentials_error_details():
    error = InvalidCredentialsError()

    assert error.details is None


def test_unauthorized_error_is_error():
    assert issubclass(UnauthorizedError, Error)


def test_unauthorized_error_type():
    error = UnauthorizedError()

    assert error.type == "unauthorized"


def test_unauthorized_error_message():
    error = UnauthorizedError()

    assert error.message == "Access to this resource is unauthorized."


def test_unauthorized_error_details():
    error = UnauthorizedError()

    assert error.details is None


def test_unauthorized_action_error_is_error():
    assert issubclass(UnauthorizedActionError, Error)


def test_unauthorized_action_error_type():
    error = UnauthorizedActionError()

    assert error.type == "unauthorized_action"


def test_unauthorized_action_error_message():
    error = UnauthorizedActionError()

    assert error.message == "You don't have the necessary authorizations to perform this action."


def test_unauthorized_action_error_details():
    error = UnauthorizedActionError()

    assert error.details is None
